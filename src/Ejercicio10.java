import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio10 {
    public static void main(String[] args) throws IOException {
        String quote = "";

        System.out.println("Ingresa una frase: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        quote = reader.readLine();
        String modifiedQuote = quote.replaceAll(" ", "");
        System.out.println(modifiedQuote);

    }
}
