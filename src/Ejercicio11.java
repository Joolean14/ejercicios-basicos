import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio11 {
    public static void main(String[] args) throws IOException {
        String quote = "";
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        int counter = 0;

        System.out.println("Ingresa una frase: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        quote = reader.readLine();

        for (int i = 0; i < vowels.length; i++) {
            for (int j = 0; j < quote.length(); j++) {
                if (vowels[i] == quote.charAt(j)) {
                    counter++;
                }
            }
            System.out.println("Hay " + counter + " " + vowels[i]);
            counter = 0;
        }


        System.out.println("La frase tiene " + quote.length() + " caracteres");


    }
}
