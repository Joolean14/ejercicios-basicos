import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio12 {
    public static void main(String[] args) throws IOException {

        String firstWord = "";
        String secondWord = "";

        System.out.println("Ingresa la primer palabra: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        firstWord = reader.readLine();
        System.out.println("Ingresa la segunda palabra: ");
        BufferedReader reader2 = new BufferedReader(new InputStreamReader(System.in));
        secondWord = reader2.readLine();

        if (firstWord.equals(secondWord)) {
            System.out.println("Las palabras son iguales");
        } else {
            for (int i = 0; i < firstWord.length(); i++) {
                if (firstWord.charAt(i) != secondWord.charAt(i)) {
                    System.out.println(firstWord.charAt(i) + " en vez de " + secondWord.charAt(i));
                }
            }
        }
    }
}
