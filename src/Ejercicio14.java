import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio14 {
    public static void main(String[] args) throws IOException {

        String userInput = "";
        int number = 0;

        System.out.println("Ingresa un numero: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        userInput = reader.readLine();
        number = Integer.parseInt(userInput);
        System.out.println(number);

        for (int i = 2; (number + i) <= 1000;) {

            System.out.println(number += i);
        }
    }
}
