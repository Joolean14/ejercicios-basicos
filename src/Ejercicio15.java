import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio15 {
    public static void main(String[] args) throws IOException {
        String message = "****** GESTION CINEMATOGRÁFICA ********\n1-Nuevo Actor\n2-Buscar Actor\n3-Eliminar Actor" +
                "\n4-Modificar Actor\n5-Ver todos los Actores\n6- Ver peliculas de los Actores\n" +
                "7-Ver categorias de las peliculas de los actores\n8-salir\n";
        String userInput = "";
        int number = 1;

        while (number != 8) {
            System.out.println(message);
            System.out.println("Ingresa un numero: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            userInput = reader.readLine();
            number = Integer.parseInt(userInput);

            if (number > 8 || number < 0) {
                System.out.println("Opcion incorrecta");
            }
            if (number == 8) {
                System.out.println("Chao!");
                break;
            }
        }

    }
}
