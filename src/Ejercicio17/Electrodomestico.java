package Ejercicio17;

public class Electrodomestico {

    protected int precioBase;
    protected String color;
    protected char consumoEnergetico;
    protected int peso;

    public Electrodomestico() {
        this.precioBase = 100;
        this.peso = 5;
        this.color = "blanco";
        this.consumoEnergetico = 'F';
    }

    public Electrodomestico(String color, char consumoEnergetico) {
        this.precioBase = 100;
        this.peso = 5;
        this.color = color;
        comprobarColor(color);
        this.consumoEnergetico = consumoEnergetico;
        comprobarConsumoEnergetico();
    }

    public Electrodomestico(int precioBase, int peso, String color, char consumoEnergetico) {
        this.precioBase = precioBase;
        this.peso = peso;
        this.color = color;
        comprobarColor(color);
        this.consumoEnergetico = consumoEnergetico;
        comprobarConsumoEnergetico();
    }

    private void comprobarConsumoEnergetico() {
        char[] ConsumosEnergeticos = {'A', 'B', 'C', 'D', 'E', 'F'};
        boolean enInventario = false;

        for (int i = 0; i < ConsumosEnergeticos.length; i++) {
            if (Character.compare(ConsumosEnergeticos[i],consumoEnergetico) == 0) {
                enInventario = true;
            }
        }

        if (enInventario) {
            this.consumoEnergetico = consumoEnergetico;
        } else {
            this.consumoEnergetico = 'F';
        }

    }

    public void comprobarColor(String color) {

        String colores[] = {"blanco", "negro", "rojo", "azul", "gris"};
        boolean enInventario = false;

        for (int i = 0; i < colores.length; i++) {
            if (colores[i].equals(color)) {
                enInventario = true;
            }
        }

        if (enInventario) {
            this.color = color;
        } else {
            this.color = "blanco";
        }

    }

    public int precioFinal() {
        switch (consumoEnergetico) {
            case 'A':
                precioBase += 100;
                break;
            case 'B':
                precioBase += 80;
                break;
            case 'C':
                precioBase += 60;
                break;
            case 'D':
                precioBase += 50;
                break;
            case 'E':
                precioBase += 30;
                break;
            case 'F':
                precioBase += 10;
                break;
            default:
                precioBase += 0;
                break;
        }
        return precioBase;
    }

    public int getPrecioBase() {
        return precioBase;
    }

    public int getPeso() {
        return peso;
    }

    public String getColor() {
        return color;
    }

    public char getConsumoEnergetico() {
        return consumoEnergetico;
    }

}
