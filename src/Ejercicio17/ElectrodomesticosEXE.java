package Ejercicio17;

public class ElectrodomesticosEXE {
    public static void main(String[] args) {

        Electrodomestico ElectrodomesticosCreados[] = new Electrodomestico[10];

        ElectrodomesticosCreados[0] = new Television();
        ElectrodomesticosCreados[1] = new Television();
        ElectrodomesticosCreados[2] = new Electrodomestico("rojo", 'A');
        ElectrodomesticosCreados[3] = new Lavadora(200, 43);
        ElectrodomesticosCreados[4] = new Lavadora(400, 100, 'A', "verde", 15);
        ElectrodomesticosCreados[5] = new Electrodomestico(340, 56, "rojo", 'C');
        ElectrodomesticosCreados[6] = new Television(250, 70);
        ElectrodomesticosCreados[7] = new Electrodomestico(600, 20, "gris", 'D');
        ElectrodomesticosCreados[8] = new Electrodomestico();
        ElectrodomesticosCreados[9] = new Lavadora(300, 40, 'Z', "blanco", 40);

        int totalElectrodomesticos = 0;
        int totalTelevisiones = 0;
        int totalLavadoras = 0;

        for (int i = 0; i < ElectrodomesticosCreados.length; i++) {

            if (ElectrodomesticosCreados[i] instanceof Electrodomestico) {
                totalElectrodomesticos += ElectrodomesticosCreados[i].precioFinal();
            }
            if (ElectrodomesticosCreados[i] instanceof Lavadora) {
                totalLavadoras += ElectrodomesticosCreados[i].precioFinal();
            }
            if (ElectrodomesticosCreados[i] instanceof Television) {
                totalTelevisiones += ElectrodomesticosCreados[i].precioFinal();
            }
        }

        System.out.println("Total del precio de las lavadoras es de " + totalLavadoras);
        System.out.println("Total del precio de las televisiones es de " + totalTelevisiones);
        System.out.println("Total del precio de los electrodomesticos es de " + totalElectrodomesticos);

    }

}
