package Ejercicio17;

public class Lavadora extends Electrodomestico {

    public int carga;

    public Lavadora(int precioBase, int peso, char consumoEnergetico, String color, int carga) {
        this.precioBase = super.precioBase;
        this.peso = super.peso;
        this.consumoEnergetico = super.consumoEnergetico;
        this.color = super.color;
        this.carga = carga;
    }

    public Lavadora(int precioBase, int peso) {
        this.precioBase = precioBase;
        this.peso = peso;
        this.consumoEnergetico = 'F';
        this.color = "blanco";
        this.carga = carga;
    }

    public Lavadora() {
        this.precioBase = 100;
        this.peso = 5;
        this.consumoEnergetico = 'F';
        this.color = "blanco";
        this.carga = 5;
    }

    public int precioFinal() {
        if (carga > 30) {
            super.precioBase += 50;
        }
        return super.precioBase;
    }

    public int getCarga() {
        return carga;
    }

}
