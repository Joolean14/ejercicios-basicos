package Ejercicio17;

public class Television extends Electrodomestico {

    private int resolucion;
    private boolean sintonizadorTDT;

    public Television() {
        this.precioBase = 100;
        this.peso = 5;
        this.consumoEnergetico = 'F';
        this.color = "blanco";
        this.resolucion = 20;
        this.sintonizadorTDT = false;
    }

    public Television(int precioBase, int peso) {
        this.precioBase = precioBase;
        this.peso = peso;
        this.consumoEnergetico = 'F';
        this.color = "blanco";
        this.resolucion = 20;
        this.sintonizadorTDT = false;
    }

    public Television(int resolucion, boolean sintonizadorTDT) {
        this.precioBase = super.precioBase;
        this.peso = super.peso;
        this.consumoEnergetico = super.consumoEnergetico;
        this.color = super.color;
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
    }

    public int precioFinal() {
        int aumentoCosto = super.precioFinal();
        if (resolucion > 40) {
            aumentoCosto += precioBase * 0.3;
        }
        if (sintonizadorTDT) {
            aumentoCosto += 50;
        }
        return aumentoCosto;
    }
}
