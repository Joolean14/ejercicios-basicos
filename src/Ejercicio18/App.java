package Ejercicio18;

public class App {
    public static void main(String[] args) {
        Serie series[] = new Serie[5];
        Videojuego videojuegos[] = new Videojuego[5];

        series[0] = new Serie();
        series[1] = new Serie("Blindspot", "Martin Gero");
        series[2] = new Serie("Friends", 25, "Humor", "Marta Kauffman");
        series[3] = new Serie("The Terror", 12, "Terror", "Soo Hugh");
        series[4] = new Serie("Seinfeld", 29, "Humor", "Jerry Seinfeld");

        videojuegos[0] = new Videojuego();
        videojuegos[1] = new Videojuego("Crash Bandicoot", 30, "Aventura", "Sony");
        videojuegos[2] = new Videojuego("Super Mario Sunshine", 100,
                "Aventura", "Nintendo");
        videojuegos[3] = new Videojuego("Fifa 21", 150, "Deportes", "EA");
        videojuegos[4] = new Videojuego("Super Smash Bros", 300);

        series[1].entregar();
        series[4].entregar();
        videojuegos[0].entregar();
        videojuegos[3].entregar();

        int entregados = 0;

        for (int i = 0; i < series.length; i++) {
            if (series[i].isEntregado()) {
                entregados += 1;
                series[i].devolver();

            }
            if (videojuegos[i].isEntregado()) {
                entregados += 1;
                videojuegos[i].devolver();
            }
        }

        System.out.println("Hay " + entregados + " articulos entregados\n");

        Serie serieMayor = series[0];
        Videojuego videojuegoMayor = videojuegos[0];


        for (int i = 1; i < series.length; i++) {
            if (series[i].compareTo(serieMayor) == Serie.MAYOR) {
                serieMayor = series[i];
            }
            if (videojuegos[i].compareTo(videojuegoMayor) == Videojuego.MAYOR) {
                videojuegoMayor = videojuegos[i];
            }

        }
            System.out.println("Video juego con  mas horas estimadas:\n" + videojuegoMayor + "\n");
            System.out.println("Serie con mas temporadas:\n" + serieMayor + "\n");

    }
}