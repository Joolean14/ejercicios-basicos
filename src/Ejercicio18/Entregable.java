package Ejercicio18;

 interface Entregable {

     public final static int MAYOR=1;
     public final static int MENOR=-1;
     public final static int IGUAL=0;

    void entregar();
     void devolver();
     boolean isEntregado();
     int compareTo(Object a);
}
