package Ejercicio18;

public class Serie implements Entregable {

    private String titulo;
    private int numeroDeTemporadas;
    private boolean entregado;
    private String genero;
    private String creador;

    public int compareTo(Object a) {
        int estado=MENOR;

        //Hacemos un casting de objetos para usar el metodo get
        Serie ref=(Serie)a;
        if (numeroDeTemporadas>ref.getnumeroTemporadas()){
            estado=MAYOR;
        }else if(numeroDeTemporadas==ref.getnumeroTemporadas()){
            estado=IGUAL;
        }

        return estado;
    }

    public Serie() {
        this.titulo = "";
        this.numeroDeTemporadas = 3;
        this.entregado = false;
        this.genero = "";
        this.creador = "";
    }

    public Serie(String titulo, String creador) {
        this.titulo = titulo;
        this.numeroDeTemporadas = 3;
        this.entregado = false;
        this.genero = "";
        this.creador = creador;
    }

    public Serie(String titulo, int numeroDeTemporadas, String genero, String creador) {
        this.titulo = titulo;
        this.numeroDeTemporadas = 3;
        this.entregado = false;
        this.genero = "";
        this.creador = creador;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getNumeroDeTemporadas() {
        return numeroDeTemporadas;
    }

    public String getGenero() {
        return genero;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setNumeroDeTemporadas(int numeroDeTemporadas) {
        this.numeroDeTemporadas = numeroDeTemporadas;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String toString() {
        return "Informacion de la Serie: \n" +
                "\tTitulo: " + titulo + "\n" +
                "\tNumero de temporadas: " + numeroDeTemporadas + "\n" +
                "\tGenero: " + genero + "\n" +
                "\tCreador: " + creador;
    }

    public void entregar() {
        this.entregado = true;
    }

    public void devolver() {
        this.entregado = false;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public int getnumeroTemporadas() {
        return numeroDeTemporadas;
    }


}
