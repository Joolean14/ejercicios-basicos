package Ejercicio18;

public class Videojuego implements Entregable {

    private String titulo;
    private int horasEstimadas;
    private boolean entregado;
    private String genero;
    private String companhia;

    @Override
    public int compareTo(Object a) {
        int estado = MENOR;

        //Hacemos un casting de objetos para usar el metodo get
        Videojuego ref = (Videojuego) a;
        if (horasEstimadas > ref.getHorasEstimadas()) {
            estado = MAYOR;
        } else if (horasEstimadas == ref.getHorasEstimadas()) {
            estado = IGUAL;
        }

        return estado;
    }

    public Videojuego() {
        this.titulo = "";
        this.horasEstimadas = 0;
        this.entregado = false;
        this.genero = genero;
        this.companhia = "";
    }

    public Videojuego(String titulo, int horasEstimadas) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.entregado = false;
        this.genero = "";
        this.companhia = "";
    }

    public Videojuego(String titulo, int horasEstimadas, String genero, String companhia) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.entregado = false;
        this.genero = genero;
        this.companhia = companhia;
    }

    public String toString() {
        return "Informacion de la Serie: \n" +
                "\tTitulo: " + titulo + "\n" +
                "\tHoras estimadas: " + horasEstimadas + "\n" +
                "\tGenero: " + genero + "\n" +
                "\tCompañia " + companhia;
    }


    public void entregar() {
        this.entregado = true;
    }

    public void devolver() {
        this.entregado = false;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

}
