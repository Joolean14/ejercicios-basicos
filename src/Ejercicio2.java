import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {

        String firstNumber = "";
        String secondNumber = "";

        System.out.println("Ingresa el primer numero:\n ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        firstNumber = reader.readLine();
        int a = Integer.parseInt(firstNumber);

        System.out.println("Ingresa el segundo numero:\n ");
        BufferedReader reader2 = new BufferedReader(new InputStreamReader(System.in));
        secondNumber = reader2.readLine();
        int b = Integer.parseInt(secondNumber);

        if (a > b) {
            System.out.println("El primer numero es mayor");
        } else if (b > a) {
            System.out.println("El segundo numero es mayor");
        } else {
            System.out.println("Las numeros son iguales");
        }

    }
}

