import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio3 {
    public static void main(String[] args) throws IOException {
        String radius = "";

        System.out.println("Ingresa el radio del circulo:\n ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        radius = reader.readLine();
        double r = Double.parseDouble(radius);
        double circleArea = Math.PI * Math.pow(r, 2);

        System.out.println("El área del círculo es: " + circleArea);


    }

}
