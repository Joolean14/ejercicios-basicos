import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio4 {
    public static void main(String[] args) throws IOException {
        String price = "";
        final double IVA = 0.21f;

        System.out.println("Ingresa el precio del producto:\n");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        price = reader.readLine();
        double p = Double.parseDouble(price);
        double finalPrice = p + (p * IVA);

        System.out.println("El precio con IVA es producto:" + finalPrice);

    }
}
