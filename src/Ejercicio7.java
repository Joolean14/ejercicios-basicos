import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio7 {
    public static void main(String[] args) throws IOException {
        String n = "";
        double number = 0;

        do {
            System.out.println("Ingresa un numero:\n");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            n = reader.readLine();
            number = Double.parseDouble(n);
        } while (number < 0);

        System.out.println(number);

    }
}
