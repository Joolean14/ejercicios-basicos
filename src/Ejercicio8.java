import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio8 {
    public static void main (String[] args) throws IOException {
        String day = "";

        System.out.println("Ingresa un dia de la semana:\n");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        day = reader.readLine();

        switch (day) {
            case "lunes":
            case "martes":
            case "miercoles":
            case "jueves":
            case "viernes":
                System.out.println("Laboral");
                break;
            case "sabado":
            case "domingo":
                System.out.println("No Laboral");
                break;
            default:
                System.out.println("Escribe el dia de la semana en minuscula y sin tildes");
        }

    }
}
