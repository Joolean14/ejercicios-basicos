import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ejercicio9 {
    public static void main(String[] args) throws IOException {
        String quote = "La sonrisa sera la mejor arma contra la tristeza";
        String modifiedQuote = quote.replaceAll("a", "e");
        String userQuote = "";


        System.out.println("Ingresa una frase: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        userQuote = reader.readLine();
        String concatQuote = modifiedQuote + " " + userQuote;
        System.out.println(concatQuote);

    }
}
