import java.util.Scanner;

public class Persona {

    private String nombre;
    private int DNI;
    private int edad;
    private float peso;
    private float altura;
    private char sexo;


    public Persona() {
        this.nombre = "";
        this.peso = 0;
        this.sexo = 'H';
        this.edad = 0;
        generaDNI();
        this.altura = 0;
    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = 0;
        this.altura = 0;
        generaDNI();
        comprobarSexo();
    }

    public Persona(String nombre, char sexo, int edad, int DNI, float peso, float altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        this.DNI = DNI;
        comprobarSexo();
    }

    public static void main(String[] args) throws Exception {

        Persona persona1 = new Persona();
        Scanner input = new Scanner(System.in);

        System.out.println("Ingrese su peso en kg:");
        persona1.peso = input.nextFloat();

        System.out.println("Ingrese su edad:");
        persona1.edad = input.nextInt();

        System.out.println("Ingrese su sexo:");
        persona1.sexo = input.next().charAt(0);

        System.out.println("Ingrese su nombre:");
        persona1.nombre = input.nextLine();

        // https://stackoverflow.com/questions/4232588/how-to-use-multiple-scanner-objects-on-system-in
        // Me genera errores, investigue un poco y aun con esto que fue lo que encontre no lo logre resolver.
        System.out.println( persona1.nombre + " " + persona1.peso + " " + persona1.edad + " " + persona1.sexo);

        Persona persona2 = new Persona();
        persona2.calcularIMC(70, 180);
        persona2.esMayorDeEdad();
        persona2.getString();

        Persona persona3 = new Persona();
        persona3.setNombre("Alberto");
        persona3.setEdad(44);
        persona3.setSexo('H');
        persona3.setPeso(34);
        persona3.setAltura(23);
        persona3.calcularIMC(34, 23);
        persona3.esMayorDeEdad();
        persona3.getString();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public void getString() {
        System.out.println("Info: \n" + nombre + "\n" + edad + "\n" + sexo + "\n" + peso + "\n" + DNI
                + "\n" + altura  + "\n\n");
    }

    private double generaDNI() {
        double aleatorioDNI = Math.floor((Math.random() + 1) * 100000000);
        System.out.println("DNI generado: " + aleatorioDNI);
        return aleatorioDNI;
    }

    public int calcularIMC(float peso, float altura) {
        final int sobrePeso = 1;
        final int idealPeso = -1;
        final int bajoPeso = 0;
        final float IMC = peso / (altura * altura);

        if (IMC < 20) {
            System.out.println("Peso ideal");
            return idealPeso;
        } else if (IMC >= 20 || IMC <= 25) {
            System.out.println("Bajo Peso");
            return bajoPeso;
        } else {
            System.out.println("Sobre peso");
            return sobrePeso;
        }
    }

    public boolean esMayorDeEdad() {
        if (edad <= 18) {
            System.out.println("Es mayor de edad");
            return true;
        } else {
            System.out.println("Es menor de edad");
            return false;
        }
    }

    private void comprobarSexo() {

        if (sexo != 'H' && sexo != 'M') {
            this.sexo = 'H';
        }
    }


}



